# btlabz-docs-umami-ha

## Overview

This documents describes possible approach to highly available, scaleable, resilient and secure umami deployment.

Notice: there is a hard requirements to have 3x nodes. I assume those nodes are separate servers.

## Solution

Here are high level solution details:

- AWS Public Cloud to deploy the solution. We leverage shared responsibility and elasticity. We also leverage managed services.
- 3x EC2 nodes in 3x separate AZ for Dashboard+API. There is not option to separate Dashboard from API right now.
- Separate database AWS RDS Postgres with MultiAZ options (standby slave)
- [Highly available VPC design](https://registry.terraform.io/modules/btower-labz/btlabz-vpc-ha-3x/aws/latest) with 3x public, 3x private, 3x database, 3x nat.
- AWS Application LB to handle ingest traffic
- AWS Managed SSL Certificates for ALB
- AWS CloudFront to cache static resources (e.g. umami.js) and improve latency for API
- AWS R53 for public DNS name, health routing, b/g deployments etc
- Autoscaling groups to recover and scale umami instances
- AWS Secrets for secrets storage and provisioning
- AWS Parameters Store for EC2 runtime configuration
- Packer+Ansible GITOPs to bake and publish umami server image. Security and dependency checks enforced.
- AWS VPC security groups to control resoruce access. Least permissions possible. E.g. direct ssh only for ops, node access only for LB, no internet for RDS etc. Control egress as well. 
- OSSEC to control server runtime configuration
- HIDS+AV agents to protect instancs at runtime
- Periodic security complicance runs on all the active hosts (e.g. AWS Inspector)
- AWS WAF on ALB and CDN
- Terraform pipelines to operate infrastructure (TFCloud/TFEnterprise)
- AD-HOS instance control and operations with ansible playbooks
- Use OWN repository with umami for ad-hoc patches and fixes
- Regular snapshot backups on RDS in addition to Multi-AZ for recovery purposes
- Regular snapshot backup of instances for investigations and compliance purposes
- Static stub pages for api/collectors and dashboard in case the main system is down
- AWS Enterprise support for production environment

## Monitoring, logging, alerting

For monitoring and logging see: [btlabz-docs-umami-monitoring](https://gitlab.com/btlabz-umami/btlabz-docs-umami-monitoring)

## Costs detailing

We should consider two types of costs: fixed and load based.

Fixed part: ALB, EC2, RDS instances vCPU, memory and sotorage.
Load based pars: ALB LCU, CLoudWatch logs and metrics ingest, traffic etc.

It's hard to determine preceise costs for the infrastructure right now as right infrastruture sizing is not known atm.

Anyway, here is the possible calculation: [calculator.aws](https://calculator.aws/#/estimate?id=dfad20b277d55b14bb4296691036c59c78402ce9)

When the target load is known and performance tests are conducted, the calcluation is going to be adjusted.

Also, it's possible to save on reserved instances when the infrastructure is stable.

## Diagrams

**Image: umami ha layout**

![umami-ha](umami-ha.png)

**Image: umami security groups layout**

![umami-ha](umami-sec.png)

## ToDo

- Umami HA POC (Terraform, Ansible, Packer).
- Performance testing to identify bottlenecks, right sizing and scaling triggers.
- Security audit and penetration testing to identify and fix security issues
- DR games and playbook (switch RDS, kill node, kill service etc)
- Research rate limiting options
- Research multi-tenancy options (on-boarding process, isolation)

## References

- [T1: apache log analisys with python, bash, sqlite and docker](https://gitlab.com/btlabz-other/btlabz-docker-httpd2sqlite-poc)
- [T2: terraform node](https://gitlab.com/btlabz-umami/terraform-aws-btlabz-umami-dev-layer) and [ansible playbook](https://gitlab.com/btlabz-umami/btlabz-ansible-umami-playbook)
- [T3: Umami HA and resilience considerations](https://gitlab.com/btlabz-umami/btlabz-docs-umami-ha)
- [T4: Umami monitoring considerations](https://gitlab.com/btlabz-umami/btlabz-docs-umami-monitoring)
